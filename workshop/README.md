# Workshop: Long-read genome assembly and pangenome graph analysis
Swiss TPH, 5.11.2024

christoph.stritt@swisstph.ch

The aim of this workshop is to get familiar with the pangenome graph framework, which, given a set of accurate assemblies, allows a full characterization of genetic variation of any type in any region of the genome.

It consists of two basic modules where we create a graph, visulize it, and call variants from it; and four modules where the graph is used to do various things:


**Basics**
*  [Create a graph and visualize it](A_create_graph.ipynb)
*  [Call variants from a graph](B_call_variants.ipynb)

**Less basic**
*  [Core genome alignment for a phylogeny](C_core_genome_alignment.ipynb)
*  [SV: insertion or deletion?](D_categorize_SVs.ipynb)
*  [Identify gene conversion events](E_gene_conversion.ipynb)
*  [Large unassembled duplications](F_large_duplications.ipynb)


## Set up
This workshop is based on Jupyiter notebooks, from where we run Python and bash code. You can run the notebooks from the sciCORE open-on-demand platform (http://ood-ubuntu.scicore.unibas.ch/) or from a editor like Visual Studio Code. 

We are going to use [PGGB](https://pggb.readthedocs.io/en/latest/) for creating analyzing pangenome graphs, a tool set developed by the human pangenome reference consortium. In addition, a few Python packages need to be installed, as explained below. 

Login to your sciCORE account and do the following:

Step 1: clone the repository from gitlab.
```
git clone https://git.scicore.unibas.ch/TBRU/PacbioSnake
```

Step 2: Copy the PGGB container into the pipeline directory
```
cd PacbioSnake
cp /scicore/home/gagneux/GROUP/PacbioSnake_resources/containers/pggb_latest.sif ./
```

Step 3: Set up a conda environment with the required Python packages. This environment can then be selected as the kernel for running the notebooks.
```
conda create -n pacbio_ws singularity=3.8.6 pandas biopython matplotlib ipykernel
```


## The data
The data we will explore in this workshop are 17 genomes that have been assembled from PacBio HiFi reads. All should be *Mycobacterium tuberculosis* strains belonging to lineage 1, and we expect them to be fairly diverse because they have been selected from across the L1 clade. Meta data for these genomes can be found here: 

These genomes have neither been published nor thoroughly analyzed, so genuine discoveries are possible during the workshop!


## More modules to come...
- Mapping long reads against the assemblies to assess assembly accuracy
- Mapping short reads against a graph

