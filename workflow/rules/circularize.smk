
rule circlator_mapreads:
    input: 
        assembly = config["outdir"] + "/{sample}/flye/assembly.fasta",
        reads = lambda wildcards: expand(samples[wildcards.sample].longread_fastq)
    output: config["outdir"] + "/{sample}/circlator/01.mapreads.bam"
    params:
        threads = config["threads_per_job"]

    shell:
        """
        circlator mapreads {input.assembly} {input.reads} {output} --threads {params.threads}

        """

rule circlator_bam2reads:
    input: config["outdir"] + "/{sample}/circlator/01.mapreads.bam"
    output: config["outdir"] +"/{sample}/circlator/02.bam2reads.fasta"
    params:
        output_pref = config["outdir"]+ "/{sample}/circlator/02.bam2reads"
    shell:
        """
        circlator bam2reads {input} {params.output_pref} --discard_unmapped

        """


rule circlator_removeduplicates:
    input: config["outdir"] +"/{sample}/circlator/02.bam2reads.fasta"
    output: config["outdir"] +"/{sample}/circlator/02.bam2reads.nodup.fasta"
    run:

        seen = set()
        output_handle = open(output[0], "w")

        skip = False
        
        with open(input[0], 'r') as f:
            for line in f:
                if line.startswith('>'):

                    seq_name = line[1:].strip()

                    if seq_name not in seen:
                        seen.add(seq_name)
                        output_handle.write(line)
                        skip = False
                    else:
                        skip = True

                else:
                    if not skip:
                        output_handle.write(line)
        
        output_handle.close()


rule circlator_localassembly:
    input: config["outdir"] + "/{sample}/circlator/02.bam2reads.nodup.fasta"
    output: config["outdir"] + "/{sample}/circlator/03.assemble/assembly.fasta"
    params:
        outdir = config["outdir"] + "/{sample}/circlator/03.assemble",
        threads = config["threads_per_job"]
    shell:
        """
        flye --pacbio-hifi {input} --out-dir {params.outdir} --genome-size 100k --threads {params.threads}

        """

rule circlator_merge:
    input: 
        assembly = config["outdir"]  + "/{sample}/flye/assembly.fasta",
        localassembly = config["outdir"] + "/{sample}/circlator/03.assemble/assembly.fasta"
    output: config["outdir"] + "/{sample}/circlator/04.merge.fasta"
    params:
        threads = config["threads_per_job"],
        output_pref = config["outdir"] + "/{sample}/circlator/04.merge"
    shell:
        """
        circlator merge {input.assembly} {input.localassembly} {params.output_pref} --threads {params.threads}

        """

rule circlator_clean:
    input: config["outdir"] + "/{sample}/circlator/04.merge.fasta"
    output: config["outdir"] + "/{sample}/circlator/05.clean.fasta"
    params:
        output_pref = config["outdir"] + "/{sample}/circlator/05.clean"
    shell:
        """
        circlator clean {input} {params.output_pref}

        """

rule circlator_fixstart:
    input: config["outdir"] + "/{sample}/circlator/05.clean.fasta"
    output: config["outdir"] + "/{sample}/circlator/06.fixstart.fasta"
    params:
        output_pref = config["outdir"] + "/{sample}/circlator/06.fixstart"
    shell:
        """
        circlator fixstart {input} {params.output_pref}

        """
        
rule rename:
    input: 
        assembly = config["outdir"] + "/{sample}/circlator/06.fixstart.fasta"
    params:
        prefix = "{sample}",
        keep_intermediate = config["keep_intermediate"]

    output: config["outdir"]  + "/{sample}/{sample}.fasta"
    
    run:

        contig_NR = 1

        fasta_handle = open(output[0], "w")

        with open(input.assembly) as original:

            for line in original:
                if line.startswith('>'):

                    seq_name = line.split(' ')[0][1:].strip()
                    seq_name_new = '>' + params.prefix + '_' + str(contig_NR)
                    fasta_handle.write(seq_name_new + '\n')
                    contig_NR += 1

                else:
                    fasta_handle.write(line)

        fasta_handle.close()

